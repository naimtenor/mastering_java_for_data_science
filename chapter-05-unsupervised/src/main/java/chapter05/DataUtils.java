package chapter05;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.SerializationUtils;

import com.google.common.base.Stopwatch;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import com.google.common.collect.Multisets;
import com.google.common.primitives.Doubles;

import chapter05.cv.Dataset;
import joinery.DataFrame;

public class DataUtils {

	/**
	 * 데이터를 읽어 온다.
	 * 
	 * @return 데이터 셋
	 * @throws IOException
	 */
    public static Dataset readData() throws IOException {
        Path path = Paths.get("data/performance.bin");

        if (path.toFile().exists()) {
            try (InputStream os = Files.newInputStream(path)) {
                return SerializationUtils.deserialize(os);
            }
        }

        DataFrame<Object> dataframe = DataFrame.readCsv("data/performance/x_train.csv");

        DataFrame<Object> targetDf = DataFrame.readCsv("data/performance/y_train.csv");
        List<Double> targetList = targetDf.cast(Double.class).col("time");
        double[] target = Doubles.toArray(targetList);

        List<Object> memfreq = noneToNull(dataframe.col("memFreq"));
        List<Object> memtRFC = noneToNull(dataframe.col("memtRFC"));
        dataframe = dataframe.drop("memFreq", "memtRFC");
        dataframe.add("memFreq", memfreq);
        dataframe.add("memtRFC", memtRFC);

        List<Object> types = dataframe.types().stream().map(c -> c.getSimpleName()).collect(Collectors.toList());
        List<Object> columns = new ArrayList<>(dataframe.columns());
        DataFrame<Object> typesDf = new DataFrame<>();
        typesDf.add("column", columns);
        typesDf.add("type", types);

        DataFrame<Object> stringTypes = typesDf.select(p -> p.get(1).equals("String"));
        System.out.println(stringTypes.toString());

        DataFrame<Object> categorical = dataframe.retain(stringTypes.col("column").toArray());
        System.out.println(categorical);

        dataframe = dataframe.drop(stringTypes.col("column").toArray());

        for (Object column : categorical.columns()) {
            List<Object> data = categorical.col(column);
            Multiset<Object> counts = HashMultiset.create(data);
            int nunique = counts.entrySet().size();
            Multiset<Object> countsSorted = Multisets.copyHighestCountFirst(counts);

            System.out.print(column + "\t" + nunique + "\t" + countsSorted);
            List<Object> cleaned = data.stream()
                    .map(o -> counts.count(o) >= 50 ? o : "OTHER")
                    .collect(Collectors.toList());

            dataframe.add(column, cleaned);
        }

        System.out.println(dataframe.head());

        double[][] X = dataframe.toModelMatrix(0.0);
        Dataset dataset = new Dataset(X, target);

        try (OutputStream os = Files.newOutputStream(path)) {
            SerializationUtils.serialize(dataset, os);
        }

        return dataset;
    }
    
    /**
	 * 범주형 데이터를 읽어 온다.
	 * 
	 * @return 데이터 프레임
	 * @throws IOException
	 */
	public static DataFrame<Object> readCategoricalData() throws IOException {
        Path path = Paths.get("data/categorical.bin");

        if (path.toFile().exists()) {
            try (InputStream os = Files.newInputStream(path)) {
            	DfHolder holder = SerializationUtils.deserialize(os);
                DataFrame<Object> df = holder.toDf();
                return df;
            }
        }

        DataFrame<Object> dataframe = DataFrame.readCsv("data/consumer_complaints.csv");

        DataFrame<Object> categorical = dataframe.retain("product", "sub_product", "issue", "sub_issue", "company",
                "state", "zipcode", "consumer_consent_provided", "submitted_via");

        try (OutputStream os = Files.newOutputStream(path)) {
            DfHolder holder = new DfHolder(categorical);
            SerializationUtils.serialize(holder, os);
        }

        return categorical;
    }

    private static List<Object> noneToNull(List<Object> memfreq) {
        return memfreq.stream()
                .map(s -> isNone(s) ? null : Double.parseDouble(s.toString()))
                .collect(Collectors.toList());
    }

    private static boolean isNone(Object s) {
        return "None".equals(s);
    }
    
    public static DataFrame<Object> readRestOfData() throws IOException {
        Stopwatch stopwatch = Stopwatch.createStarted();

        Path path = Paths.get("data/all-complaints.bin");

        if (path.toFile().exists()) {
            try (InputStream os = Files.newInputStream(path)) {
                DfHolder holder = SerializationUtils.deserialize(os);
                DataFrame<Object> df = holder.toDf();
                System.out.println("reading dataframe from cache took " + stopwatch.stop());
                return df;
            }
        }

        DataFrame<Object> dataframe = DataFrame.readCsv("data/consumer_complaints.csv");
        System.out.println("reading dataframe took " + stopwatch.stop());

        DataFrame<Object> rest = dataframe.drop("product", "sub_product", "issue", "sub_issue", "company", "state",
                "zipcode", "consumer_consent_provided", "submitted_via");

        try (OutputStream os = Files.newOutputStream(path)) {
            DfHolder holder = new DfHolder(rest);
            SerializationUtils.serialize(holder, os);
        }

        return rest;
    }

}
