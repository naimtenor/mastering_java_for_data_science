package chapter05.cluster;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import chapter05.CommonUtils;
import chapter05.DataUtils;
import chapter05.cv.Dataset;
import chapter05.cv.Split;
import chapter05.preprocess.StandardizationPreprocessor;
import smile.clustering.KMeans;
import smile.data.SparseDataset;
import smile.regression.OLS;

public class ClusteringForSupervisedLearningTests {

	public static void main(String[] args) throws IOException {
		Dataset dataset = DataUtils.readData();

		StandardizationPreprocessor preprocessor = StandardizationPreprocessor.train(dataset);
		dataset = preprocessor.transform(dataset);

		Split split = dataset.trainTestSplit(0.3);
		Dataset train = split.getTrain();

		double[][] X = train.getX();

		// 1. 특성 공학을 위한 클러스터링
		System.out.println("======================== 특성 공학을 위한 클러스터링");
		clusteringAsFeature(X);
		
		// 2. 차원 축소를 위한 클러스터링
		System.out.println("======================== 차원 축소를 위한 클러스터링");
		clusteringASDR(X, train, split);
		
		// 3. 지도 학습을 위한 클러스터링
		System.out.println("======================== 지도 학습을 위한 클러스터링");
		clusteringASSupervisedLearning(X, train, split);
	}
	
	/**
	 * 특성 공학을 위한 클러스터링
	 * 
	 * @param X 데이터
	 */
	private static void clusteringAsFeature(double[][] X) {
		int k = 60;
        int maxIter = 10;
        int runs = 1;
        
		KMeans km = new KMeans(X, k, maxIter, runs);
		int[] labels = km.getClusterLabel();
		SparseDataset sparse = new SparseDataset(k);
		for (int i = 0; i < labels.length; i++) {
			sparse.set(i, labels[i], 1.0);
		}
	}
	
	/**
	 * 차원 축소를 위한 클러스터링
	 * 
	 * @param X 데이터
	 * @param train 훈련 데이터
	 * @param split 테스트를 위한 데이터
	 */
	private static void clusteringASDR(double[][] X, Dataset train, Split split) {
		int k = 60;
		int maxIter = 10;
		int runs = 1;

		KMeans km = new KMeans(X, k, maxIter, runs);

		double[][] centroids = km.centroids();

		double[][] distances = distance(X, centroids);

		train = new Dataset(distances, train.getY());

		OLS finalModel = new OLS(train.getX(), train.getY());

		Dataset test = split.getTest();
		double[][] testDistancse = distance(test.getX(), centroids);
		test = new Dataset(testDistancse, test.getY());

		double testRmse = CommonUtils.rmse(finalModel, test);
		System.out.printf("final model rmse=%.4f%n", testRmse);
	}
	
	/**
	 * 지도 학습을 위한 클러스터링
	 * 
	 * @param X 데이터
	 * @param train 훈련 데이터
	 * @param split 테스트를 위한 데이터
	 */
	private static void clusteringASSupervisedLearning(double[][] X, Dataset train, Split split) {
		int k = 250;
		int maxIter = 10;
		int runs = 1;
		
		KMeans km = new KMeans(X, k, maxIter, runs);
		
		double[] y = train.getY();
		int[] labels = km.getClusterLabel();
		Multimap<Integer, Double> groups = ArrayListMultimap.create();
		for (int i = 0; i < labels.length; i++) {
			groups.put(labels[i], y[i]);
		}
		Map<Integer, Double> meanValue = new HashMap<>();
		for (int i = 0; i < k; i++) {
			double mean = groups.get(i).stream().mapToDouble(d -> d).average().getAsDouble();
			meanValue.put(i, mean);
		}
		
		Dataset test = split.getTest();
		double[][] testX = test.getX();
        int[] testLabels = Arrays.stream(testX).mapToInt(km::predict).toArray();

        double[] testPredict = Arrays.stream(testLabels).mapToDouble(meanValue::get).toArray();
        double result = CommonUtils.rmse(test.getY(), testPredict);
        System.out.printf("result rmse: %.4f%n", result);
	}
	
	/**
	 * 두 벡터 간 거리를 구한다.
	 * 
	 * @param A 벡터 A
	 * @param B 벡터 B
	 * @return 거리 벡터
	 */
	public static double[][] distance(double[][] A, double[][] B) {
		double[] squaredA = squareRows(A);
		double[] squaredB = squareRows(B);

		Array2DRowRealMatrix mA = new Array2DRowRealMatrix(A, false);
		Array2DRowRealMatrix mB = new Array2DRowRealMatrix(B, false);
		double[][] product = mA.multiply(mB.transpose()).getData();

		int nrow = product.length;
		int ncol = product[0].length;
		double[][] distance = new double[nrow][ncol];
		for (int i = 0; i < nrow; i++) {
			for (int j = 0; j < ncol; j++) {
				double dist = squaredA[i] - 2 * product[i][j] + squaredB[j];
				distance[i][j] = Math.sqrt(dist);
			}
		}

		return distance;
	}
	
	/**
	 * 개별 벡터의 노름을 구한다.
	 * 
	 * @param data 데이터
	 * @return 벡터 노름
	 */
	private static double[] squareRows(double[][] data) {
		int nrow = data.length;
		double[] squared = new double[nrow];
		for (int i = 0; i < nrow; i++) {
			double[] row = data[i];
			double res = 0.0;
			for (int j = 0; j < row.length; j++) {
				res = res + row[j] * row[j];
			}
			squared[i] = res;
		}
		return squared;
	}

}
