package chapter05.cluster;

import java.awt.Dimension;
import java.io.IOException;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;

import chapter05.DataUtils;
import chapter05.dr.Projections;
import chapter05.preprocess.SmileOHE;
import joinery.DataFrame;
import smile.clustering.HierarchicalClustering;
import smile.clustering.linkage.Linkage;
import smile.clustering.linkage.UPGMALinkage;
import smile.data.SparseDataset;
import smile.math.matrix.SingularValueDecomposition;
import smile.plot.Dendrogram;
import smile.plot.PlotCanvas;

public class HierarchicalMethodTests {

	public static void main(String[] args) throws IOException {

		// 1. 10000개의 데이터를 샘플링한다.
		System.out.println("======================== 10000개의 데이터를 샘플링한다.");
		DataFrame<Object> categorical = DataUtils.readCategoricalData();
		SparseDataset sparse = SmileOHE.oneHotEncoding(categorical);

		SingularValueDecomposition svd = SingularValueDecomposition.decompose(sparse.toSparseMatrix(), 30);

		double[][] data = Projections.project(sparse, svd.getV());
		data = sample(data, 10000);

		// 2. 개별 벡터의 크기를 구한다.
		System.out.println("======================== 개별 벡터의 크기를 구한다.");
		double[] norms = squareRows(data);

		// 3. 유클리디안 거리를 활용하여 근접 행렬을 구한다.
		System.out.println("======================== 유클리디안 거리를 활용하여 근접 행렬을 구한다.");
		double[][] distEuclidean = calcualateSquaredEuclidean(data);
		
		// 4. 코사인 거리를 활용하여 근접 행렬을 구한다.
		System.out.println("======================== 코사인 거리를 활용하여 근접 행렬을 구한다.");
//		double[][] distCosine = calcualateCosineDistance(data);
		
		// 5. 클러스터링을 수행한다.
		System.out.println("======================== 클러스터링을 수행한다.");
		Linkage linkage = new UPGMALinkage(distEuclidean);
		HierarchicalClustering hc = new HierarchicalClustering(linkage);
		
		// 6. 덴드로그램을 그린다.
		System.out.println("======================== 덴드로그램을 그린다.");
//		showDendrogram(hc);
		
		// 7.1 특정 거리 기준으로 클러스터를 자른다.
		System.out.println("======================== 특정 거리 기준으로 클러스터를 자른다.");
		double height = 1.0;
		int[] labels1 = hc.partition(height);
		System.out.println(labels1);
		
		//7.2 클러스터 기준으로 자른다.
		System.out.println("======================== 클러스터 기준으로 자른다.");
		int k = 5;
		int[] labels2 = hc.partition(k);
		System.out.println(labels2);
	}
	
	/**
	 * 덴드로그램을 그린다.
	 * @param hc 클러스터링 데이터
	 */
	private static void showDendrogram(HierarchicalClustering hc) {
        JFrame frame = new JFrame("Dendrogram");
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        PlotCanvas dendrogram = Dendrogram.plot(hc.getTree(), hc.getHeight());
        frame.add(dendrogram);

        frame.setSize(new Dimension(1000, 1000));
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
	
	/**
	 * 데이터를 샘플링한다.
	 * 
	 * @param data 인코딩 된 데이터
	 * @param size 샘플링 사이즈
	 * @return 샘플링 데이터
	 */
	private static double[][] sample(double[][] data, int size) {
        return sample(data, size, System.nanoTime());
    }
	
	/**
	 * 데이터를 샘플링 한다. 랜덤 추출을 사용한다.
	 * 
	 * @param data 인코딩 된 데이터
	 * @param size 샘플링 사이즈
	 * @param seed 랜덤 객체 생성을 위한 시드
	 * @return 샘플링 데이터
	 */
	private static double[][] sample(double[][] data, int size, long seed) {
        Random rnd = new Random(seed);

        int[] idx = rnd.ints(0, data.length).distinct().limit(size).toArray();
        double[][] sample = new double[size][];
        for (int i = 0; i < size; i++) {
            sample[i] = data[idx[i]];
        }

        return sample;
    }
	
	/**
	 * 개별 벡터의 노름을 구한다.
	 * 
	 * @param data 데이터
	 * @return 벡터 노름
	 */
	private static double[] squareRows(double[][] data) {
        int nrow = data.length;

        double[] squared = new double[nrow];
        for (int i = 0; i < nrow; i++) {
            double[] row = data[i];

            double res = 0.0;
            for (int j = 0; j < row.length; j++) {
                res = res + row[j] * row[j];
            }

            squared[i] = res;
        }

        return squared;
    }
	
	/**
	 * 유클리디안 거리를 구한다.
	 * 
	 * @param data 샘플링 데이터
	 * @return 근접 행렬
	 */
	public static double[][] calcualateSquaredEuclidean(double[][] data) {
        int nrow = data.length;

        double[] squared = squareRows(data);

        Array2DRowRealMatrix m = new Array2DRowRealMatrix(data, false);
        double[][] product = m.multiply(m.transpose()).getData();
        double[][] dist = new double[nrow][nrow];

        for (int i = 0; i < nrow; i++) {
            for (int j = i + 1; j < nrow; j++) {
                double d = squared[i] - 2 * product[i][j] + squared[j];
                dist[i][j] = dist[j][i] = d; 
            }
        }

        return dist;
    }
	
	/**
	 * 코사인 거리를 구한다.
	 * 
	 * @param data 샘플링 데이터
	 * @return 근접 행렬
	 */
	public static double[][] calcualateCosineDistance(double[][] data) {
        int nrow = data.length;
        double[][] normalized = normalize(data);

        Array2DRowRealMatrix m = new Array2DRowRealMatrix(normalized, false);
        double[][] cosine = m.multiply(m.transpose()).getData();

        for (int i = 0; i < nrow; i++) {
            double[] row = cosine[i];
            for (int j = 0; j < row.length; j++) {
                row[j] = 1 - row[j];
            }
        }

        return cosine;
    }
	
	/**
	 * 데이터 정규화 
	 * 
	 * @param data 샘플링 데이터
	 * @return 정규화 데이터
	 */
	private static double[][] normalize(double[][] data) {
        int nrow = data.length;
        double[][] normalized = new double[nrow][];

        for (int i = 0; i < nrow; i++) {
            double[] row = data[i].clone();
            normalized[i] = row;
            double norm = new ArrayRealVector(row, false).getNorm();
            for (int j = 0; j < row.length; j++) {
                row[j] = row[j] / norm;
            }
        }

        return normalized;
    }

}
