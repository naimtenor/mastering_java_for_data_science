package chapter05.cluster;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Random;

import chapter05.DataUtils;
import chapter05.dr.Projections;
import chapter05.preprocess.JsatOHE;
import chapter05.preprocess.SmileOHE;
import joinery.DataFrame;
import jsat.SimpleDataSet;
import jsat.classifiers.DataPoint;
import jsat.clustering.SeedSelectionMethods.SeedSelection;
import jsat.clustering.kmeans.ElkanKMeans;
import jsat.linear.distancemetrics.EuclideanDistance;
import smile.clustering.KMeans;
import smile.clustering.XMeans;
import smile.data.SparseDataset;
import smile.math.matrix.SingularValueDecomposition;

public class PartitioningMethodTests {

	public static void main(String[] args) throws IOException {
		// 1. 데이터를 읽어 특이 값 분해 한다.
		System.out.println("======================== 데이터를 읽어 특이 값 분해 한다.");
		DataFrame<Object> categorical = DataUtils.readCategoricalData();
		SparseDataset sparse = SmileOHE.oneHotEncoding(categorical);

		SingularValueDecomposition svd = SingularValueDecomposition.decompose(sparse.toSparseMatrix(), 30);

		double[][] proj = Projections.project(sparse, svd.getV());
		
		// 2. 설정 매개변수를 정한다. KMeans 객체를 생성한다.
		System.out.println("======================== 설정 매개변수를 정한다. KMeans 객체를 생성한다.");
		int k = 10;
		int maxIter = 100;
		int runs = 3;
		KMeans km = new KMeans(proj, k, maxIter, runs);
		
		// 번외. JSAT을 사용한 클러스터링
		System.out.println("======================== 번외. JSAT을 사용한 클러스터링");
//		usingJSAT(categorical);
		
		// 3.1 팔꿈치 메소드를 이용한 K 값 추측
		System.out.println("======================== 팔꿈치 메소드를 이용한 K 값 추측");
		checkElbowMethod(proj);
		
		// 3.2 X-평균 사용 K 값 추측
		System.out.println("======================== X-평균 사용 K 값 추측");
		checkXMeans(proj);
		
	}
	
	/**
	 * JSAT을 사용한 클러스터링
	 * 
	 * @param categorical 범주형 데이터
	 */
	private static void usingJSAT(DataFrame<Object> categorical) {
		SimpleDataSet ohe = JsatOHE.oneHotEncoding(categorical);
		EuclideanDistance distance = new EuclideanDistance();
		Random rand = new Random(1);
		SeedSelection seedSelection = SeedSelection.RANDOM;
		jsat.clustering.kmeans.KMeans kmjsat = new ElkanKMeans(distance, rand, seedSelection);
		List<List<DataPoint>> clusteringjsat = kmjsat.cluster(ohe);
	}
	
	/**
	 * 팔꿈치 메소드 계산
	 * 
	 * @param proj 투영 데이터
	 * @throws FileNotFoundException
	 */
	private static void checkElbowMethod(double[][] proj) throws FileNotFoundException {
		PrintWriter out = new PrintWriter("distortion.txt");
		for (int k = 3; k < 50; k++) {
		    int maxIter = 100;
		    int runs = 3;
		    KMeans km = new KMeans(proj, k, maxIter, runs);
		    out.println(k + "\t" + km.distortion());
		}
		out.close();
	}
	
	/**
	 * X-평균 사용 K 값 예측
	 * 
	 * @param proj 투영 데이터
	 */
	private static void checkXMeans(double[][] proj) {
		int kmax = 300;
		XMeans km = new XMeans(proj, kmax);
		System.out.println("selected number of clusters: " + km.getNumClusters());
	}

}
