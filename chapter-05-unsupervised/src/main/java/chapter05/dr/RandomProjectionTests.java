package chapter05.dr;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

import chapter05.CommonUtils;
import chapter05.DataUtils;
import chapter05.cv.Dataset;
import chapter05.cv.Split;
import chapter05.preprocess.SmileOHE;
import joinery.DataFrame;
import no.uib.cipr.matrix.DenseMatrix;
import no.uib.cipr.matrix.sparse.CompRowMatrix;
import no.uib.cipr.matrix.sparse.FlexCompRowMatrix;
import smile.data.SparseDataset;
import smile.math.SparseArray;
import smile.math.SparseArray.Entry;
import smile.projection.RandomProjection;
import smile.regression.OLS;

public class RandomProjectionTests {

	public static void main(String[] args) throws IOException {
		// 1. 데이터를 읽어 온다.
		System.out.println("======================== 데이터를 읽어 온다.");
		Dataset dataset = DataUtils.readData();
		
		// 2. 랜덤 투영 객체를 생성한다.
		System.out.println("======================== 랜덤 투영 객체를 생성한다.");
		double[][] X = dataset.getX();
		int inputDimension = X[0].length;
		int outputDimension = 100;
		int seed = 1;
		
		smile.math.Math.setSeed(seed);
		RandomProjection rp = new RandomProjection(inputDimension, outputDimension);
		
		// 3. 성능 예측 문제에 랜덤 투영를 사용
		System.out.println("======================== 성능 예측 문제에 랜덤 투영를 사용");
		useRandomProjectionWithOLS(dataset, rp, X);
		
		// 4. 구현 적합성 확인
		System.out.println("======================== 구현 적합성 확인");
		sanityCheck(inputDimension, outputDimension, seed, dataset, rp, X);
		
		// 5. 범주형 데이터 테스트
		System.out.println("======================== 범주형 데이터 테스트");
		categorical();
	}
	
	/**
	 * 최소자승법 회귀 문제에 랜덤 투영 사용
	 * 
	 * @param dataset 원본 데이터 셋
	 * @param rp 랜덤 투영 기저
	 * @param X 원본 X 데이터
	 */
	private static void useRandomProjectionWithOLS(Dataset dataset, RandomProjection rp, double[][] X) {
		double[][] projected = rp.project(X);
		dataset = new Dataset(projected, dataset.getY());
		Split trainTestSplit = dataset.shuffleSplit(0.3);
		Dataset train = trainTestSplit.getTrain();
		List<Split> folds = train.shuffleKFold(3);
		DescriptiveStatistics ols = CommonUtils.crossValidate(folds, data -> {
			return new OLS(data.getX(), data.getY());
		});
		System.out.printf("ols: rmse=%.4f &pm; %.4f%n", ols.getMean(), ols.getStandardDeviation());
	}
	
	/**
	 * 구현 적합성 확인
	 * 
	 * @param inputDimension 투영 할 행렬의 차수
	 * @param outputDimension 투영 될 행렬의 차수
	 * @param seed 재현성 확보를 위한 임의 숫자 생성기의 시드
	 * @param dataset 원본 데이터 셋
	 * @param rp 랜덤 투영 기저
	 * @param X 원본 X 데이터
	 */
	private static void sanityCheck(int inputDimension, int outputDimension, int seed, Dataset dataset, RandomProjection rp, double[][] X) {
		double[][] basis = randomProjection(inputDimension, outputDimension, seed);
		double[][] projected = project(X, basis);
		dataset = new Dataset(projected, dataset.getY());
		Split trainTestSplit = dataset.shuffleSplit(0.3);
		Dataset train = trainTestSplit.getTrain();
		List<Split> folds = train.shuffleKFold(3);
		DescriptiveStatistics ols = CommonUtils.crossValidate(folds, data -> {
			return new OLS(data.getX(), data.getY());
		});
		System.out.printf("ols: rmse=%.4f &pm; %.4f%n", ols.getMean(), ols.getStandardDeviation());
	}
	
	/**
	 * 범주형 데이터 테스트
	 * 
	 * @throws IOException
	 */
	private static void categorical() throws IOException {
		DataFrame<Object> categorical = DataUtils.readCategoricalData();
		SparseDataset sparse = SmileOHE.hashingEncoding(categorical, 50_000);
		double[][] basis =randomProjection(50_000, 100, 0);
		double[][] proj =project(sparse, basis);
	}

	/**
	 * 랜덤 기저 행렬을 만들기 위해 가우시안 분포에서 데이터 샘플링
	 * 
	 * @param inputDimension 원본 데이터 차수
	 * @param outputDimension 축소 공간 희망 차수
	 * @param seed 재현성 확보를 위한 시드
	 * @return 샘플링 데이터
	 */
	public static double[][] randomProjection(int inputDimension, int outputDimension, int seed) {
        NormalDistribution normal = new NormalDistribution(0.0, 1.0 / outputDimension);
        normal.reseedRandomGenerator(seed);
        double[][] result = new double[inputDimension][];

        for (int i = 0; i < inputDimension; i++) {
            result[i] = normal.sample(outputDimension);
        }

        return result;
    }
	
	/**
	 * 행렬 X를 기저로 투영
	 * 
	 * @param Xd 원본 행렬
	 * @param Vd 기저 행렬
	 * @return 투영 데이터
	 */
	public static double[][] project(double[][] Xd, double[][] Vd) {
        DenseMatrix X = new DenseMatrix(Xd);
        DenseMatrix V = new DenseMatrix(Vd);

        DenseMatrix XV = new DenseMatrix(X.numRows(), V.numColumns());
        X.mult(V, XV);

        return to2d(XV);
    }

	/**
	 * 생성된 기저에 희소 행렬 투영
	 * 
	 * @param dataset 희소 행렬
	 * @param Vd 기저
	 * @return 투영 데이터
	 */
    public static double[][] project(SparseDataset dataset, double[][] Vd) {
        CompRowMatrix X = asRowMatrix(dataset);
        DenseMatrix V = new DenseMatrix(Vd);

        DenseMatrix XV = new DenseMatrix(X.numRows(), V.numColumns());
        X.mult(V, XV);

        return to2d(XV);
    }

    /**
     * 밀집 행렬을 2차원 더블형 배열로 변환
     * 
     * @param XV 밀집 행렬
     * @return 2차원 더블형 배열
     */
    public static double[][] to2d(DenseMatrix XV) {
        double[] data = XV.getData();
        int nrows = XV.numRows();
        int ncols = XV.numColumns();
        double[][] result = new double[nrows][ncols];

        for (int col = 0; col < ncols; col++) {
            for (int row = 0; row < nrows; row++) {
                result[row][col] = data[row + col * nrows];
            }
        }

        return result;
    }

    private static CompRowMatrix asRowMatrix(SparseDataset dataset) {
        int ncols = dataset.ncols();
        int nrows = dataset.size();
        FlexCompRowMatrix X = new FlexCompRowMatrix(nrows, ncols);

        SparseArray[] array = dataset.toArray(new SparseArray[0]);
        for (int rowIdx = 0; rowIdx < array.length; rowIdx++) {
            Iterator<Entry> row = array[rowIdx].iterator();
            while (row.hasNext()) {
                Entry entry = row.next();
                X.set(rowIdx, entry.i, entry.x);
            }
        }

        return new CompRowMatrix(X);
    }
}
