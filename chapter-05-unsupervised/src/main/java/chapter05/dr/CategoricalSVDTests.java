package chapter05.dr;

import java.io.IOException;
import java.util.Iterator;

import chapter05.DataUtils;
import chapter05.preprocess.SmileOHE;
import joinery.DataFrame;
import no.uib.cipr.matrix.DenseMatrix;
import no.uib.cipr.matrix.sparse.CompRowMatrix;
import no.uib.cipr.matrix.sparse.FlexCompRowMatrix;
import smile.data.SparseDataset;
import smile.math.SparseArray;
import smile.math.SparseArray.Entry;
import smile.math.matrix.SingularValueDecomposition;
import smile.math.matrix.SparseMatrix;

public class CategoricalSVDTests {

	public static void main(String[] args) throws IOException {
		// 1. 범주형 데이터 읽어오기
		System.out.println("======================== 범주형 데이터 읽어오기");
		DataFrame<Object> categorical = DataUtils.readCategoricalData();

		// 2. 특성 해싱
		System.out.println("======================== 특성 해싱");
		SparseDataset sparse = SmileOHE.hashingEncoding(categorical, 50_000);

		// 3. 열 기반의 데이터 형태로 변환
		System.out.println("======================== 열 기반의 데이터 형태로 변환");
		SparseMatrix matrix = sparse.toSparseMatrix();
		SingularValueDecomposition svd = SingularValueDecomposition.decompose(matrix, 100);

		// 4. MJT 기반의 행렬 표현식으로 변환
		System.out.println("======================== MJT 기반의 행렬 표현식으로 변환");
		CompRowMatrix X = convertToMTJMatrix(sparse);

		// 5. 밀집 행렬 V 생성
		System.out.println("======================== 밀집 행렬 V 생성");
		DenseMatrix V = new DenseMatrix(svd.getV());
		
		// 6. X와 V를 곱한다.
		System.out.println("======================== X와 V를 곱한다.");
		DenseMatrix XV = new DenseMatrix(X.numRows(), V.numColumns());
		X.mult(V, XV);
		
		// 7. 결과 데이터를 정리한다.
		System.out.println("======================== 결과 데이터를 정리한다.");
		double[][] result = representResult(XV);

	}
	
	
	
	/**
	 * 스마일의 {@link SparseDataset} 을 MTJ의 {@link CompRowMatrix} 로 변환한다.
	 * @param sparse 스마일 {@link SparseMatrix}
	 * @return MTJ의 {@link CompRowMatrix}
	 */
	private static CompRowMatrix convertToMTJMatrix(SparseDataset sparse) {
		int ncols = sparse.ncols();
		int nrows = sparse.size();
		FlexCompRowMatrix builder = new FlexCompRowMatrix(nrows, ncols);
		SparseArray[] array = sparse.toArray(new SparseArray[0]);
		for (int rowIdx = 0; rowIdx < array.length; rowIdx++) {
			Iterator<Entry> row = array[rowIdx].iterator();
			while (row.hasNext()) {
				Entry entry = row.next();
				builder.set(rowIdx, entry.i, entry.x);
			}
		}
		CompRowMatrix X = new CompRowMatrix(builder);
		
		return X;
	}
	
	/**
	 * 결과 데이터 변환
	 * 
	 * @param XV 밀집 행렬
	 * @return 일반적인 형태의 결과 값
	 */
	private static double[][] representResult(DenseMatrix XV) {
		double[] data = XV.getData();
		int nrows = XV.numRows();
		int ncols = XV.numColumns();
		double[][] result = new double[nrows][ncols];
		for (int col = 0; col < ncols; col++) {
		    for (int row = 0; row < nrows; row++) {
		        result[row][col] = data[row + col * nrows];
		    }
		}

		return result;
	}

}
