package chapter05.dr;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

import chapter05.CommonUtils;
import chapter05.DataUtils;
import chapter05.cv.Dataset;
import chapter05.cv.Split;
import chapter05.preprocess.StandardizationPreprocessor;
import smile.projection.PCA;
import smile.regression.OLS;

public class PCATests {

	public static void main(String[] args) throws IOException {
		
		// 1. 데이터를 읽는다.
		Dataset dataset = DataUtils.readData();
		
		// 2. StandardizationPreprocessor를 사용하여 데이터 표준화
		StandardizationPreprocessor preprocessor = StandardizationPreprocessor.train(dataset);
		dataset = preprocessor.transform(dataset);
		
		// 3. 누적 분산 확인
		PCA pca = new PCA(dataset.getX(),  false);
		double[] variance = pca.getCumulativeVarianceProportion();
		System.out.println(Arrays.toString(variance));
		
		// 4. 최소자승법(OLS) 수행
		Split trainTestSplit = dataset.shuffleSplit(0.3);
        Dataset train = trainTestSplit.getTrain();
        List<Split> folds = train.shuffleKFold(3);
        DescriptiveStatistics ols = CommonUtils.crossValidate(folds, data -> {
        	return new OLS(data.getX(), data.getY());
        });
        
        double meanols = ols.getMean();
		double stdold = ols.getStandardDeviation();
		System.out.printf("ols: rmse=%.4f &pm; %.4f%n", meanols, stdold);

		// 5. 주 성분 분석 수행
        double[] ratios = { 0.95, 0.99, 0.999 };
		for (double ratio : ratios) {
			pca = pca.setProjection(ratio);
			double[][] projectedX = pca.project(train.getX());
			Dataset projected = new Dataset(projectedX, train.getY());
			folds = projected.shuffleKFold(3);
			ols = CommonUtils.crossValidate(folds, data -> {
				return new OLS(data.getX(), data.getY());
			});
			double mean = ols.getMean();
			double std = ols.getStandardDeviation();
			System.out.printf("ols (%.3f): rmse=%.4f &pm; %.4f%n", ratio, mean, std);
		}
	}
}
