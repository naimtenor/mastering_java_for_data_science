package chapter05.dr;

import java.io.IOException;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.DiagonalMatrix;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

import chapter05.DataUtils;
import chapter05.cv.Dataset;
import chapter05.preprocess.StandardizationPreprocessor;
import smile.math.matrix.Matrix;
import smile.math.matrix.SingularValueDecomposition;

public class TruncatedSVDTests {

	public static void main(String[] args) throws IOException {
		
		// 1. 데이터를 읽는다.
		System.out.println("======================== 데이터를 읽는다.");
		Dataset dataset = DataUtils.readData();
		
		// 2. StandardizationPreprocessor를 사용하여 데이터 표준화
		System.out.println("======================== StandardizationPreprocessor를 사용하여 데이터 표준화");
		StandardizationPreprocessor preprocessor = StandardizationPreprocessor.train(dataset);
		dataset = preprocessor.transform(dataset);
		
		// 3. 특이 값 분해(SVD)를 수행한다.
		System.out.println("======================== 특이 값 분해(SVD)를 수행한다.");
		double[][] X = dataset.getX(); // X 는 평균 중심화 데이터
		Matrix matrix = new Matrix(X);
		SingularValueDecomposition svd = SingularValueDecomposition.decompose(matrix, 100);
		
		// 5. 행렬의 곱 수행 U × S
		System.out.println("======================== 행렬의 곱 수행 U × S");
		double[][] usdata = US(svd);
		
		// 6. 행렬의 곱 수행 X × V
		System.out.println("======================== 행렬의 곱 수행 X × V");
		double[][] xvdata = XV(dataset.getX(), svd.getV());
		
		// 7. 전체 분산 계산
		System.out.println("======================== 전체 분산 계산");
		double totalVariance = getTotalVariance(dataset);
		
		// 8. 누적 비율 계산
		System.out.println("======================== 누적 비율 계산");
		double[] cumulatedRatio = getCumulatedRatio(totalVariance, svd, dataset.getX());
	}
	
	/**
	 * 누적 비율을 구한다.
	 * 
	 * @param totalVariance 전체 분산 값
	 * @param svd 특이 값 분해 객체
	 * @param X X 데이터
	 */
	public static double[] getCumulatedRatio(double totalVariance, SingularValueDecomposition svd, double[][] X) {
		int nrows = X.length;
		double[] singularValues = svd.getSingularValues();
		double[] cumulatedRatio = new double[singularValues.length];
		double acc = 0.0;
		for (int i = 0; i < singularValues.length; i++) {
		    double s = singularValues[i];
		    double ratio = (s * s / nrows) / totalVariance;
		    acc = acc + ratio;
		    cumulatedRatio[i] = acc;
		}
		
		return cumulatedRatio;
	}
	
	/**
	 * 전체 분산 값을 구한다.
	 * 
	 * @param dataset 원본 데이터
	 * @return 전체 분산 값
	 */
	public static double getTotalVariance(Dataset dataset) {
		Array2DRowRealMatrix matrix = new Array2DRowRealMatrix(dataset.getX(), false);
		int ncols = matrix.getColumnDimension();
		double totalVariance = 0.0;
		for (int col = 0; col < ncols; col++) {
			double[] column = matrix.getColumn(col);
			DescriptiveStatistics stats = new DescriptiveStatistics(column);
			totalVariance = totalVariance + stats.getVariance();
		}
		
		return totalVariance;
	}
	
	/**
	 * U × S 실행
	 * @param svd 특이 값 분해 클래스
	 * @return 곱셈 결과
	 */
	public static double[][] US(SingularValueDecomposition svd) {
        DiagonalMatrix S = new DiagonalMatrix(svd.getSingularValues());
        Array2DRowRealMatrix U = new Array2DRowRealMatrix(svd.getU(), false);

        RealMatrix result = S.multiply(U.transpose()).transpose();
        return result.getData();
    }

	/**
	 * X × V 실행
	 * @param Xd X 데이터
	 * @param Vd V 데이터
	 * @return 곱셈 결과
	 */
    public static double[][] XV(double[][] Xd, double[][] Vd) {
        Array2DRowRealMatrix X = new Array2DRowRealMatrix(Xd, false);
        Array2DRowRealMatrix V = new Array2DRowRealMatrix(Vd, false);
        return X.multiply(V).getData();
    }

}
