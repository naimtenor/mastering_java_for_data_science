package chapter05;

import java.util.List;
import java.util.function.Function;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

import chapter05.cv.Dataset;
import chapter05.cv.Split;
import smile.regression.Regression;
import smile.validation.MSE;

public class CommonUtils {

	/**
	 * 교차 검증을 수행한다.
	 * 
	 * @param folds fold 값
	 * @param trainer 교차 검증 함수
	 * @return 통계 요약 값
	 */
	public static DescriptiveStatistics crossValidate(List<Split> folds, Function<Dataset, Regression<double[]>> trainer) {
        double[] aucs = folds.parallelStream().mapToDouble(fold -> {
            Dataset train = fold.getTrain();
            Dataset validation = fold.getTest();
            Regression<double[]> model = trainer.apply(train);
            return rmse(model, validation);
        }).toArray();

        return new DescriptiveStatistics(aucs);
    }
	
	/**
	 * 평가를 위해 평균 제곱근 편차를 구한다.
	 *  
	 * @param truth 실제 값
	 * @param prediction 예측 값
	 * @return 평균 제곱근 편차
	 */
	public static double rmse(double[] truth, double[] prediction) {
        double mse = new MSE().measure(truth, prediction);
        return Math.sqrt(mse);
    }
	
	/**
	 * 평가를 위해 평균 제곱근 편차를 구한다.
	 *  
	 * @param model 회귀 모델
	 * @param dataset 데이터
	 * @return 평균 제곱근 편차
	 */
	public static double rmse(Regression<double[]> model, Dataset dataset) {
        double[] prediction = predict(model, dataset);
        double[] truth = dataset.getY();

        double mse = new MSE().measure(truth, prediction);
        return Math.sqrt(mse);
    }
	
	/**
	 * 모델을 사용하여 예측치 데이터를 생성한다.
	 * 
	 * @param model 회귀 모델
	 * @param dataset 데이터
	 * @return 예측치
	 */
	public static double[] predict(Regression<double[]> model, Dataset dataset) {
        double[][] X = dataset.getX();
        double[] result = new double[X.length];

        for (int i = 0; i < X.length; i++) {
            result[i] = model.predict(X[i]);
        }

        return result;
    }
	
}
